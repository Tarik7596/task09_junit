package com.babii;

import com.babii.controller.GameDriver;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;

@RunWith(MockitoJUnitRunner.class)
public class GameDriverTests {
    @InjectMocks
    private GameDriver driverMock;

    @Test
    public void SetFieldTrue () throws Exception {
        Field fToTest = driverMock.getClass().getDeclaredField("isFieldCreated");
        fToTest.setAccessible(true);
        driverMock.createMineField(10, 10, 0.3);
        Assert.assertTrue(fToTest.getBoolean(driverMock));
    }
}
