package com.babii.controller;

interface Performable {
    void createMineField(int x, int y, double p);
    String showMineLoc();
    String showGameView();
}
