package com.babii.controller;

import com.babii.utils.TxtConst;

public class FieldNotCreatedException extends RuntimeException {
    public String toString() {
        return TxtConst.FIELD_EXC_MSG;
    }
}
