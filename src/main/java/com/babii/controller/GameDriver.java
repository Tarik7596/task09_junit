package com.babii.controller;

import com.babii.model.MineField;

public class GameDriver implements Performable{
    private boolean isFieldCreated;
    private MineField field;

    public GameDriver() {
        isFieldCreated = false;
    }

    public void createMineField(int x, int y, double p) {
        field = new MineField(x, y, p);
        isFieldCreated = true;
    }

    public String showMineLoc() throws FieldNotCreatedException {
        StringBuilder sb = new StringBuilder();
        if(!isFieldCreated) {
            throw new FieldNotCreatedException();
        } else {
            for(String[] ss : field.getMineLocArr()) {
                for(String s : ss) {
                    sb.append(s);
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public String showGameView() throws FieldNotCreatedException {
        StringBuilder sb = new StringBuilder();
        if(!isFieldCreated) {
            throw new FieldNotCreatedException();
        } else {
            for(int[] ss : field.getGameplayArr()) {
                for(int s : ss) {
                    sb.append(s);
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
