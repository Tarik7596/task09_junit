package com.babii.view;

@FunctionalInterface
public interface Executable {
    void execute();
}
