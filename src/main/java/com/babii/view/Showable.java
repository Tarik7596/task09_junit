package com.babii.view;

public interface Showable {
    void show(String s);
}
